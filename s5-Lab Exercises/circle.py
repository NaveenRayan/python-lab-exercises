import random

class Circle():

    def __init__(self, radius):
        self.radius = radius
    

    def get_circle_area(self):
        return (3.14 * self.radius**2)

    
    def get_circle_circumference(self):
        return ((2 * 3.14) * self.radius)

for i in range(100):
    r = (random.randint(1,10))
    circle = Circle(r)
    print("area when radius is {}= ".format(r),circle.get_circle_area())
    print("circumference when radius is {}= ".format(r),circle.get_circle_circumference())
