def get_circle_area(radius):  # function to fine area of circle
    area = (3.14 * (eval(radius) * eval(radius)))
    return area


def get_circle_circumference(radius):   # function to fine circumference of circle
    circumference = (2 * 3.14 * eval(radius))
    return circumference
