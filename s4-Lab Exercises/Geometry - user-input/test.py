import circle
import rectangle


# input radius for calculating area and circumference of circle
def test_circle():
    radius = input("enter the radius value: ")
    if radius.isdigit():
        area = circle.get_circle_area(radius)
        print("area of the circle is :", area)

        circumference = circle.get_circle_circumference(radius)
        print("circumference of the circle is :", circumference)


    else:
        print("please enter valid integer as input")



# input length and  breadth  for calculating area and perimeter of rectangle



def test_rectangle():
    length = input("enter the length of rectangle: ")
    
    if length.isdigit():
        breadth = input("enter the breath of rectangle: ")
        if breadth.isdigit():
            area = rectangle.get_area_rectangle(length, breadth)
            print("area of the rectangle is :", area)
            perimeter = rectangle.get_perimeter_rectangle(length, breadth)
            print("perimeter of the rectangle is :", perimeter)

    else:
        print("please enter valid integer as input")

