def get_circle_area(r):  #function to fine area of circle
    a = 3.14 * (r*r)
    return a


def get_circle_circumference(r): #function to fine circumference of circle
    c = 2 * 3.14 * r
    return c


